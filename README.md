# Home Automation - Home Assistant

[[_TOC_]]

## Description

A [Home Assistant](https://www.home-assistant.io/) instance for providing home automation.
