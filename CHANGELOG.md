# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2022-05-07]

### Added
- Moved to new repo group

### Removed
- Removed updater as it has now been removed from HA
