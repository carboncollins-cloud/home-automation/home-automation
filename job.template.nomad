job "home-automation" {
  name = "Home Automation (Home Assistant)"
  type = "service"
  region = "[[ .defaultRegion ]]"
  datacenters = ["[[ .defaultDatacenter ]]"]
  priority = "[[ .defaultPriority ]]"
  namespace = "home-automation"

  vault {
    policies = ["job-home-automation"]
  }

  group "home-assistant" {
    count = 1

    constraint {
      attribute = "${attr.unique.hostname}"
      value = "proxima-a"
    }

    volume "home-assistant" {
      type = "host"
      read_only = false
      source = "home-assistant"
    }

    ephemeral_disk {
      size = 300
      sticky = true
      migrate = true
    }

    network {
      mode = "bridge"
    }

    service {
      name = "home-assistant"
      port = "8123"
      task = "core"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "ups-1-nut"
              local_bind_port = 3493
            }

            upstreams {
              destination_name = "ups-2-nut"
              local_bind_port = 3494
            }

            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }

      check {
        expose = true
        name = "Application Health Status"
        type = "http"
        path = "/manifest.json"
        interval = "10s"
        timeout = "3s"
      }

      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.hass.entrypoints=https",
        "traefik.http.routers.hass.rule=Host(`hass.hq.carboncollins.se`)",
        "traefik.http.routers.hass.tls=true",
        "traefik.http.routers.hass.tls.certresolver=lets-encrypt",
        "traefik.http.routers.hass.tls.domains[0].main=*.hq.carboncollins.se"
      ]
    }

    task "core" {
      driver = "docker"

      volume_mount {
        volume = "home-assistant"
        destination = "/config"
        read_only = false
      }

      config {
        image = "[[ .imageName ]]"

        volumes = [
          "local/hass/config/configuration.yaml:/config/configuration.yaml",
          "local/hass/config/packages:/config/packages",
          "local/hass/config/blueprints/automation/hq:/config/blueprints/automation/hq",
          "secrets/secrets.yaml:/config/secrets.yaml"
        ]
      }

      template {
        data = <<EOH
          TZ='[[ .defaultTimezone ]]'
        EOH

        destination = "secrets/environment.env"
        env = true
      }

      # This currently requires the job to initially be deployed twice to copy the source into the volume
      artifact {
        source = "[[ .artifactUrl ]]"
        destination = "local/hass"

        options {
          archive = "zip"
        }
      }

      template {
        data = <<EOF
{{ with secret "jobs/home-automation/tasks/core/secrets" }}
{{ .Data.data | toYAML }}
{{ end }}
EOF
        destination = "secrets/secrets.yaml"
        change_mode = "restart"
        
      }

      resources {
        cpu = 1000
        memory = 2048
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
